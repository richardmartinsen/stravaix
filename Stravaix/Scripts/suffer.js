﻿$(document).ready(function () {

    $.getJSON("/Suffer/GetJsonResult", function (result) {
        var data = result.map(function (point) { return { date: point.Dato, Score: point.Score, TssWatt: point.TssWatt, TssPuls: point.TssPuls } });
        console.log(data);
        updatedata(data);
    });

    function updatedata(newData) {
    if ($('.pages_dashboard').length) {
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        //var testdato = new Date('2015-01-01'.getTime());
        //console.log(testdato);
        if ($('.ld-widget-main__chart').length) {
            Morris.Line({
                element: $('.ld-widget-main__chart'),
                data: newData,

                //data: [
				//	{ d: new Date('2015-01-01').getTime(), a: 15, b: 5, c: 75 },
				//	{ d: new Date('2015-02-01').getTime(), a: 60, b: 15, c: 90 },
				//	{ d: new Date('2015-03-01').getTime(), a: 30, b: 10, c: 80 },
				//	{ d: new Date('2015-04-01').getTime(), a: 50, b: 20, c: 90 },
				//	{ d: new Date('2015-05-01').getTime(), a: 35, b: 10, c: 95 },
				//	{ d: new Date('2015-06-01').getTime(), a: 90, b: 5, c: 15 },
				//	{ d: new Date('2015-07-01').getTime(), a: 35, b: 15, c: 50 },
				//	{ d: new Date('2015-08-01').getTime(), a: 50, b: 10, c: 100 },
				//	{ d: new Date('2015-09-01').getTime(), a: 30, b: 5, c: 75 },
				//	{ d: new Date('2015-10-01').getTime(), a: 95, b: 15, c: 30 },
				//	{ d: new Date('2015-11-01').getTime(), a: 30, b: 20, c: 45 }
                //],
                xkey: 'date',
                ykeys: ['Score', 'TssWatt', 'TssPuls'],
                dateFormat: function (x) {
                    return new Date(x).toDateString();
                },
                //xLabelFormat: function (x) {
                //    return month[new Date(x).getMonth()];
                //},
                hoverCallback: function (Score, TssWatt, TssPuls) {
                    return "Suffer : " + Score + " TssWatt : " + TssWatt + " TssPuls : " + TssPuls;
                },
                labels: ['Score', 'TssWatt', 'TssPuls'],
                lineColors: ['#ed4949', '#FED42A', '#20c05c', '#1e59d9'],
                pointSize: 0,
                pointStrokeColors: ['#ed4949', '#FED42A', '#20c05c', '#1e59d9'],
                lineWidth: 3,
                smooth: false,
                continuousLine: false,
                resize: true
            });
        }
    }
    }
});