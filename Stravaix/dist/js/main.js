/*
 *
 *   Right - Responsive Admin Template
 *   v 0.1.0
 *   http://adminbootstrap.com
 *
 */

$(document).ready(function() {
	$('.sidebar li').on('click', function() {
		var second_nav = $(this).find('.collapse').first();
		if (second_nav.length) {
			second_nav.collapse('toggle');
			$(this).toggleClass('opened');
		}
	});

	$('.scrollable').scrollbar();
	$('.selectize-dropdown-content').addClass('scrollbar-macosx').scrollbar();
	$('.nav-pills, .nav-tabs').tabdrop();

	$('body').on('click', '.header-navbar-mobile__menu button', function() {
		console.log($(this));
		$('.dashboard').toggleClass('dashboard_menu');
	});
});