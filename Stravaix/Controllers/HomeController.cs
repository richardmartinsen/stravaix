﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stravaix.Controllers
{
    using System.Collections;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;

    using Stravaix.Models;
    using Stravaix.StravaApi.Activities;
    using Stravaix.StravaApi.Athletes;
    using Stravaix.StravaApi.Authentication;
    using Stravaix.StravaApi.Clients;
    using Stravaix.StravaApi.Streams;

    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}