﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Stravaix.Controllers
{
    using System.Collections;
    using System.Data.Entity.Migrations;
    using System.Threading.Tasks;

    using Microsoft.Ajax.Utilities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;

    using Stravaix.Models;
    using Stravaix.StravaApi.Activities;
    using Stravaix.StravaApi.Athletes;
    using Stravaix.StravaApi.Authentication;
    using Stravaix.StravaApi.Clients;
    using Stravaix.StravaApi.Streams;

    public class SufferController : Controller
    {
        private StravaixContext db = new StravaixContext();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> About()
        {
            ViewBag.Message = "Your application description page.";

            //Task<List<Comment>> b = StravaTest();
            //List<Comment> c = b.Result;

            var b = getAthleteSummary();

            return View(b);
        }

        public JsonResult GetJsonResult()
        {
            var b = getAthleteSummary();
            return Json(b, JsonRequestBehavior.AllowGet);
        }

        public void InsertSummary(ActivitySummary summary)
        {
            //db.ActivityMetas.up AddOrUpdate(summary);
            db.ActivitySummaries.AddOrUpdate(summary);
            db.SaveChanges();

        }

        public void InsertSuffer(SufferScore scores)
        {
            db.SufferScores.AddOrUpdate(scores);
            db.SaveChanges();

        }

        private IEnumerable<SufferScore> getAthleteSummary()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            StaticAuthentication auth = new StaticAuthentication(user.StravaKey);
            //StaticAuthentication auth = new StaticAuthentication("72da1a028657d1d0a5c628c87373bebdf15151db");

            // bb636aa7c25ee744f9c961ef3e7979ed56adaee2 martin
            // 72da1a028657d1d0a5c628c87373bebdf15151db richard

            StravaClient client = new StravaClient(auth);
            StreamClient stream = new StreamClient(auth);
            List<SufferScore> stats = new List<SufferScore>();

            int ftp = 0;

            if (User.Identity.Name == "martin@hoff.no")
            {
                ftp = 370;
            }
            else
            {
                ftp = 300;
            }

            List<ActivitySummary> listAkt = client.Activities.GetActivitiesAfter(DateTime.Now.AddDays(-35));
            int i = 0;
            foreach (var actSum in listAkt)
            {
                //if (actSum.Type == ActivityType.Ride)
                //{
                    //this.InsertSummary(actSum);
                    SufferScore score = new SufferScore();
                    List<ActivityZone> actSoner = client.Activities.GetActivityZones(actSum.Id.ToString());
                    
                    double tssPulsTall = 0;
                    foreach (var zone in actSoner)
                    {
                        if (zone.Type == "heartrate")
                        {
                            if (zone.Score == null)
                            {
                                score.Score = 0;
                            }
                            else
                            {
                                score.Score = (int)zone.Score;    
                            }
                            
                            int sone = 0;
                            
                            foreach (var buckets in zone.Buckets)
                            {
                                sone++;
                                tssPulsTall += (sone * 25) * (buckets.Time / (double)3600);
                                //if (buckets.Minimum <= actSum.AverageHeartrate
                                //    && buckets.Maximum >= actSum.AverageHeartrate)
                                //{
                                    
                                //}
                                //var o = buckets.Minimum;
                            }
                        }
                    }
                    //var puls = actSoner.First();
                    var p = 1;
                    //var suffer = puls.y;
                    i += 1;
                    //DateTime dt = Convert.ToDateTime(actSum.StartDate);
                    score.Dato = actSum.StartDate;
                    //score.Dato = EpochTimeExtensions.ToEpochTime(dt) * 1000;
                    //if (puls.Score == null)
                    //{
                    //    score.Score = 0;
                    //}

                    //else
                    //{
                    //    score.Score = (int)puls.Score;
                    //}
                //switch (@enum)
                //{
                        
                //} 

                    double d = (actSum.MovingTimeSpan.TotalSeconds * actSum.WeightedAverageWatts * (actSum.WeightedAverageWatts / (double)ftp)) / (ftp * (double)3600) * 100;
                    score.TssWatt = (int)d;
                
                    
                    //double tssPuls = (70/(double)3600 * actSum.MovingTimeSpan.TotalSeconds) * 1.07;
                    score.TssPuls = (int)tssPulsTall;

                    score.x = i;
                    //if (score.Score != 0 && score.TssPuls != 0 && score.TssWatt != 0)
                    //{
                        stats.Add(score);
                        //this.InsertSuffer(score);
                    //}
                    
                //}
            }
            return stats.ToArray();
        }

        //private async static Task<AthleteSummary> StravaTest()
        private async Task<List<Comment>> StravaTest()
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            StaticAuthentication auth = new StaticAuthentication(user.StravaKey);
            //StaticAuthentication auth = new StaticAuthentication("72da1a028657d1d0a5c628c87373bebdf15151db");
            StravaClient client = new StravaClient(auth);

            //List<Comment> comments = await client.GetCommentsAsync("102162300");
            //Activity athlete = await client.GetActivityAsync("102162300");
            //Athlete athlete = await client.GetAthleteAsync("1985994");
            //AthleteSummary a = await client.Athletes.GetAthleteAsync();
            var ass = getAthleteSummary();
            List<Comment> comments = await client.Activities.GetCommentsAsync("447602106");
            //AthleteSummary a = await client.Athletes.GetAthleteAsync(ass.Id.ToString());
            return comments;
        }
    }

    public static class EpochTimeExtensions
    {
        /// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTime dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given date value to epoch time.
        /// </summary>
        public static long ToEpochTime(this DateTimeOffset dateTime)
        {
            var date = dateTime.ToUniversalTime();
            var ticks = date.Ticks - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).Ticks;
            var ts = ticks / TimeSpan.TicksPerSecond;
            return ts;
        }

        /// <summary>
        /// Converts the given epoch time to a <see cref="DateTime"/> with <see cref="DateTimeKind.Utc"/> kind.
        /// </summary>
        public static DateTime ToDateTimeFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddTicks(timeInTicks);
        }

        /// <summary>
        /// Converts the given epoch time to a UTC <see cref="DateTimeOffset"/>.
        /// </summary>
        public static DateTimeOffset ToDateTimeOffsetFromEpoch(this long intDate)
        {
            var timeInTicks = intDate * TimeSpan.TicksPerSecond;
            return new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero).AddTicks(timeInTicks);
        }
    }
}