﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stravaix.Models
{
    public class SufferScore
    {
        public int id { get; set; }

        public int x { get; set; }

        public int Score { get; set; }

        public int TssWatt { get; set; }

        public int TssPuls { get; set; }

        public string Dato { get; set; }

    }
}