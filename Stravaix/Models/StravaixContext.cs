﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Stravaix.Models
{
    using System.Data.Entity;

    using Stravaix.StravaApi.Activities;
    using Stravaix.StravaApi.Athletes;

    public class StravaixContext : DbContext
    {
        public DbSet<ActivitySummary> ActivitySummaries { get; set; }

        public DbSet<AthleteMeta> AthleteMetas { get; set; }

        public DbSet<ActivityMeta> ActivityMetas { get; set; }

        public DbSet<SufferScore> SufferScores { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //Database.SetInitializer(new StravaixContext());

        //    modelBuilder.Entity<ActivityMeta>().HasKey(q => new { q.Id });
        //    modelBuilder.Entity<ActivitySummary>().HasKey(c => new { c.Id });
        //    base.OnModelCreating(modelBuilder);
        //}
    }
}